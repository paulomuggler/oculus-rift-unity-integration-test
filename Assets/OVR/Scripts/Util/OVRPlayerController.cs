/************************************************************************************

Copyright   :   Copyright 2014 Oculus VR, LLC. All Rights reserved.

Licensed under the Oculus VR Rift SDK License Version 3.2 (the "License");
you may not use the Oculus VR Rift SDK except in compliance with the License,
which is provided at the time of installation or download, or which
otherwise accompanies this software in either electronic or hard copy form.

You may obtain a copy of the License at

http://www.oculusvr.com/licenses/LICENSE-3.2

Unless required by applicable law or agreed to in writing, the Oculus VR SDK
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

************************************************************************************/

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Controls the player's movement in virtual reality.
/// </summary>
[RequireComponent(typeof(CharacterController))]
public class OVRPlayerController : MonoBehaviour
{
	/// <summary>
	/// The rate acceleration during movement.
	/// </summary>
	public float Acceleration = 0.1f;

	/// <summary>
	/// The rate of damping on movement.
	/// </summary>
	public float Damping = 0.3f;

	/// <summary>
	/// The rate of additional damping when moving sideways or backwards.
	/// </summary>
	public float BackAndSideDampen = 0.5f;

	/// <summary>
	/// The force applied to the character when jumping.
	/// </summary>
	public float JumpForce = 0.3f;

	/// <summary>
	/// The rate of rotation when using a gamepad.
	/// </summary>
	public float RotationAmount = 1.0f;

	/// <summary>
	/// The rate of rotation when using the keyboard.
	/// </summary>
	public float RotationRatchet = 45.0f;

	/// <summary>
	/// If true, reset the initial pitch of the player controller when the Hmd pose is recentered.
	/// </summary>
	public bool HmdResetsX = true;

	/// <summary>
	/// If true, reset the initial yaw of the player controller when the Hmd pose is recentered.
	/// </summary>
	public bool HmdResetsY = true;

	/// <summary>
	/// If true, tracking data from a child OVRCameraRig will update the direction of movement.
	/// </summary>
	public bool HmdRotatesX = true;

	/// <summary>
	/// If true, tracking data from a child OVRCameraRig will update the direction of movement.
	/// </summary>
	public bool HmdRotatesY = true;

	/// <summary>
	/// Modifies the strength of gravity.
	/// </summary>
	public float GravityModifier = 0.379f;
	
	/// <summary>
	/// If true, the OVRPlayerController will use the player's profile data for height, eye depth, etc.
	/// </summary>
	public bool useProfileData = true;

	protected CharacterController Controller = null;
	protected OVRCameraRig CameraRig = null;

	private float MoveScale = 1.0f;
	private Vector3 MoveThrottle = Vector3.zero;
	private float FallSpeed = 0.0f;
	private OVRPose? InitialPose;
	private float InitialXRotation = 0.0f;
	private float InitialYRotation = 0.0f;
	private float MoveScaleMultiplier = 1.0f;
	private float RotationScaleMultiplier = 1.0f;
	private bool  SkipMouseRotation = true;
	private bool  HaltUpdateMovement = false;
	private bool prevHatLeft = false;
	private bool prevHatRight = false;
	private float SimulationRate = 60f;
	private Transform graphics;

	void Awake()
	{
		Controller = gameObject.GetComponent<CharacterController>();

		if(Controller == null)
			Debug.LogWarning("OVRPlayerController: No CharacterController attached.");

		graphics = transform.Find ("Graphics");

		// We use OVRCameraRig to set rotations to cameras,
		// and to be influenced by rotation
		OVRCameraRig[] CameraRigs = gameObject.GetComponentsInChildren<OVRCameraRig>();

		if(CameraRigs.Length == 0)
			Debug.LogWarning("OVRPlayerController: No OVRCameraRig attached.");
		else if (CameraRigs.Length > 1)
			Debug.LogWarning("OVRPlayerController: More then 1 OVRCameraRig attached.");
		else
			CameraRig = CameraRigs[0];

		InitialXRotation = transform.rotation.eulerAngles.x;
		InitialYRotation = transform.rotation.eulerAngles.y;
	}

	void OnEnable()
	{
		OVRManager.display.RecenteredPose += ResetOrientation;

		if (CameraRig != null)
		{
			CameraRig.UpdatedAnchors += UpdateTransform;
		}
	}

	void OnDisable()
	{
		OVRManager.display.RecenteredPose -= ResetOrientation;

		if (CameraRig != null)
		{
			CameraRig.UpdatedAnchors -= UpdateTransform;
		}
	}

	protected virtual void Update()
	{
		if (useProfileData)
		{
			if (InitialPose == null)
			{
				InitialPose = new OVRPose()
				{
					position = CameraRig.transform.localPosition,
					orientation = CameraRig.transform.localRotation
				};
			}

			var p = CameraRig.transform.localPosition;
			p.y = OVRManager.profile.eyeHeight - 0.5f * Controller.height;
			p.z = OVRManager.profile.eyeDepth;
			CameraRig.transform.localPosition = p;
		}
		else if (InitialPose != null)
		{
			CameraRig.transform.localPosition = InitialPose.Value.position;
			CameraRig.transform.localRotation = InitialPose.Value.orientation;
			InitialPose = null;
		}

		UpdateMovement();

		Vector3 moveDirection = Vector3.zero;

		float motorDamp = (1.0f + (Damping * SimulationRate * Time.deltaTime));

		MoveThrottle.x /= motorDamp;
		MoveThrottle.y /= motorDamp;
		MoveThrottle.z /= motorDamp;

		moveDirection += MoveThrottle * SimulationRate * Time.deltaTime;

		// Gravity
//		if (Controller.isGrounded && FallSpeed <= 0)
//			FallSpeed = ((Physics.gravity.y * (GravityModifier * 0.002f)));
//		else
//			FallSpeed += ((Physics.gravity.y * (GravityModifier * 0.002f)) * SimulationRate * Time.deltaTime);
//
//		moveDirection.y += FallSpeed * SimulationRate * Time.deltaTime;

		// Offset correction for uneven ground
		float bumpUpOffset = 0.0f;

//		if (Controller.isGrounded && MoveThrottle.y <= 0.001f)
//		{
//			bumpUpOffset = Mathf.Max(Controller.stepOffset, new Vector3(moveDirection.x, 0, moveDirection.z).magnitude);
//			moveDirection -= bumpUpOffset * Vector3.up;
//		}

		Vector3 predictedXZ = Vector3.Scale((Controller.transform.localPosition + moveDirection), new Vector3(1, 0, 1));

		// Move contoller
		Controller.Move(moveDirection);

		Vector3 actualXZ = Vector3.Scale(Controller.transform.localPosition, new Vector3(1, 0, 1));

		if (predictedXZ != actualXZ)
			MoveThrottle += (actualXZ - predictedXZ) / (SimulationRate * Time.deltaTime);
	}

	public float maxPitchRate = 5f;
	public float maxYawRate = 5f;

	public float pitchAccelerationMax = 1f;
	public float yawAccelerationMax = 1f;

//	public float pitchAccTime = .333f;
//	public float yawAccTime = .333f;
//
//	float pitchAccTimer = 0;
//	float yawAccTimer = 0;

	float rightAxisX, rightAxisY;
//	float prevRightAxisX, prevRightAxisY;
//	float dRightAxisX, dRightAxisY;
//	float prevDRightAxisX, prevDRightAxisY;
//	float dRightAxisXSign, dRightAxisYSign;
//	float prevDRightAxisXSign, prevDRightAxisYSign;

	public virtual void UpdateMovement()
	{
		if (HaltUpdateMovement)
			return;


		MoveScale = 1.0f * MoveScaleMultiplier;
		MoveScale *= SimulationRate * Time.deltaTime;

		RotationAmount = 1.0f * RotationScaleMultiplier;
		RotationAmount *= SimulationRate * Time.deltaTime;

		// Compute this for key movement
		float moveInfluence = Acceleration * MoveScale;

		Quaternion ort = transform.rotation;
//		Vector3 ortEuler = ort.eulerAngles;
//		ortEuler.z = ortEuler.x = 0f;
//		ort = Quaternion.Euler(ortEuler);

//		bool curHatLeft = OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.LeftShoulder);

		Vector3 euler = transform.rotation.eulerAngles;

//		float rotateInfluence = SimulationRate * Time.deltaTime * RotationAmount * RotationScaleMultiplier;

//		moveInfluence = SimulationRate * Time.deltaTime * Acceleration * 0.1f * MoveScale * MoveScaleMultiplier;

		MoveThrottle += ort * (moveInfluence * Vector3.forward);

		Transform root = CameraRig.trackingSpace;
		Transform centerEye = CameraRig.centerEyeAnchor;

//		prevRightAxisX = rightAxisX;
//		prevRightAxisY = rightAxisY;

		rightAxisX = Mathf.DeltaAngle(root.rotation.eulerAngles.y, centerEye.rotation.eulerAngles.y);
		rightAxisY = Mathf.DeltaAngle(root.rotation.eulerAngles.x, centerEye.rotation.eulerAngles.x);

//		prevDRightAxisY = dRightAxisY;
//		prevDRightAxisX = dRightAxisX;
//
//		prevDRightAxisYSign = Mathf.Sign (dRightAxisY);
//		prevDRightAxisXSign = Mathf.Sign (dRightAxisX);
//
//		dRightAxisY = rightAxisY - prevRightAxisY;
//		dRightAxisX = rightAxisX - prevRightAxisX;
//
//		dRightAxisYSign = Mathf.Sign (dRightAxisY);
//		dRightAxisXSign = Mathf.Sign (dRightAxisX);

//		Debug.Log ("pitch In: "+rightAxisY);
//		Debug.Log ("yaw In: "+rightAxisX);

//		if(prevDRightAxisYSign != dRightAxisYSign){
//			pitchAccTimer = 0;
//		}
//		if(prevDRightAxisXSign != dRightAxisXSign){
//			yawAccTimer = 0;
//		}

		Vector3 gEuler = graphics.localRotation.eulerAngles;
		graphics.localRotation = Quaternion.Euler (rightAxisY, gEuler.y, -2*rightAxisX);

		float pitchRate = Mathf.LerpAngle (0, maxPitchRate, Mathf.Abs(rightAxisY) / 180f)*Mathf.Sign(rightAxisY);
		float yawRate = Mathf.LerpAngle (0, maxYawRate, Mathf.Abs(rightAxisX) / 180f)*Mathf.Sign(rightAxisX);

//		float pitchRate = Mathf.Clamp (rightAxisX, -maxYawRate, maxYawRate);
//		float yawRate = Mathf.Clamp (rightAxisY, -maxPitchRate, maxPitchRate);

//		yawAccTimer += Time.deltaTime;
//		pitchAccTimer += Time.deltaTime;

//		float pitchAcc = Extensions.SmootherStep (0, pitchAccelerationMax, pitchAccTimer, pitchAccTime);
//		float yawAcc = Extensions.SmootherStep (0, yawAccelerationMax, yawAccTimer, yawAccTime);

//		rightAxisX *= yawAcc;
//		rightAxisY *= pitchAcc;

		euler.y += yawRate * RotationAmount;
		euler.x += pitchRate * RotationAmount;
//		euler.z += rightAxisX;
//		euler.y = Mathf.MoveTowardsAngle (euler.y, euler.y + rightAxisX, 3f);
//		euler.x = Mathf.MoveTowardsAngle (euler.x, euler.x + rightAxisY, 3f);

		transform.rotation = Quaternion.Euler(euler);
	}

	/// <summary>
	/// Invoked by OVRCameraRig's UpdatedAnchors callback. Allows the Hmd rotation to update the facing direction of the player.
	/// </summary>
	public void UpdateTransform(OVRCameraRig rig)
	{
		Transform root = CameraRig.trackingSpace;
		Transform centerEye = CameraRig.centerEyeAnchor;

		if (HmdRotatesY)
		{
			Vector3 prevPos = root.position;
			Quaternion prevRot = root.rotation;

			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, centerEye.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);

			root.position = prevPos;
			root.rotation = prevRot;
		}

		if (HmdRotatesX)
		{
			Vector3 prevPos = root.position;
			Quaternion prevRot = root.rotation;
			
			transform.rotation = Quaternion.Euler(centerEye.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
			
			root.position = prevPos;
			root.rotation = prevRot;
		}
	}

	/// <summary>
	/// Jump! Must be enabled manually.
	/// </summary>
	public bool Jump()
	{
		if (!Controller.isGrounded)
			return false;

		MoveThrottle += new Vector3(0, JumpForce, 0);

		return true;
	}

	/// <summary>
	/// Stop this instance.
	/// </summary>
	public void Stop()
	{
		Controller.Move(Vector3.zero);
		MoveThrottle = Vector3.zero;
		FallSpeed = 0.0f;
	}

	/// <summary>
	/// Gets the move scale multiplier.
	/// </summary>
	/// <param name="moveScaleMultiplier">Move scale multiplier.</param>
	public void GetMoveScaleMultiplier(ref float moveScaleMultiplier)
	{
		moveScaleMultiplier = MoveScaleMultiplier;
	}

	/// <summary>
	/// Sets the move scale multiplier.
	/// </summary>
	/// <param name="moveScaleMultiplier">Move scale multiplier.</param>
	public void SetMoveScaleMultiplier(float moveScaleMultiplier)
	{
		MoveScaleMultiplier = moveScaleMultiplier;
	}

	/// <summary>
	/// Gets the rotation scale multiplier.
	/// </summary>
	/// <param name="rotationScaleMultiplier">Rotation scale multiplier.</param>
	public void GetRotationScaleMultiplier(ref float rotationScaleMultiplier)
	{
		rotationScaleMultiplier = RotationScaleMultiplier;
	}

	/// <summary>
	/// Sets the rotation scale multiplier.
	/// </summary>
	/// <param name="rotationScaleMultiplier">Rotation scale multiplier.</param>
	public void SetRotationScaleMultiplier(float rotationScaleMultiplier)
	{
		RotationScaleMultiplier = rotationScaleMultiplier;
	}

	/// <summary>
	/// Gets the allow mouse rotation.
	/// </summary>
	/// <param name="skipMouseRotation">Allow mouse rotation.</param>
	public void GetSkipMouseRotation(ref bool skipMouseRotation)
	{
		skipMouseRotation = SkipMouseRotation;
	}

	/// <summary>
	/// Sets the allow mouse rotation.
	/// </summary>
	/// <param name="skipMouseRotation">If set to <c>true</c> allow mouse rotation.</param>
	public void SetSkipMouseRotation(bool skipMouseRotation)
	{
		SkipMouseRotation = skipMouseRotation;
	}

	/// <summary>
	/// Gets the halt update movement.
	/// </summary>
	/// <param name="haltUpdateMovement">Halt update movement.</param>
	public void GetHaltUpdateMovement(ref bool haltUpdateMovement)
	{
		haltUpdateMovement = HaltUpdateMovement;
	}

	/// <summary>
	/// Sets the halt update movement.
	/// </summary>
	/// <param name="haltUpdateMovement">If set to <c>true</c> halt update movement.</param>
	public void SetHaltUpdateMovement(bool haltUpdateMovement)
	{
		HaltUpdateMovement = haltUpdateMovement;
	}

	/// <summary>
	/// Resets the player look rotation when the device orientation is reset.
	/// </summary>
	public void ResetOrientation()
	{
		if (HmdResetsX)
		{
			Vector3 euler = transform.rotation.eulerAngles;
			euler.x = InitialXRotation;
			transform.rotation = Quaternion.Euler(euler);
		}

		if (HmdResetsY)
		{
			Vector3 euler = transform.rotation.eulerAngles;
			euler.y = InitialYRotation;
			transform.rotation = Quaternion.Euler(euler);
		}
	}
}

