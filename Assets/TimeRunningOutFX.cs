﻿using UnityEngine;
using System.Collections;

public class TimeRunningOutFX : MonoBehaviour {

	public float thresholdSeconds = 4.0f;
	public GameTimer countdown;
	public Animator animator;
	
	void Start () {
	}

	void FixedUpdate () {
		if(countdown.elapsed <= thresholdSeconds){
//			Debug.Log ("Time Running Out.");
			animator.SetBool("timeRunningOut", true);
		}else{
//			Debug.Log ("Plenty of Time.");
			animator.SetBool("timeRunningOut", false);
		}
	}
}
