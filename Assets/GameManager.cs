﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	public OVRPlayerController ovrPlayer;

	public GameTimer countdown, elapsed;

	void Start () {
		if (ovrPlayer == null)
			ovrPlayer = GameObject.FindObjectOfType<OVRPlayerController> ();

		StartCoroutine (WaitForHSWDisplay ());
		NotificationCenter.DefaultCenter.AddObserver (this, "onPlayerCollectedSunring");
		NotificationCenter.DefaultCenter.AddObserver (this, "onGameTimeDepleted");
	}

	void onPlayerCollectedSunring(NotificationCenter.Notification n){
		SunRingCollect sunring = n.sender as SunRingCollect;
		countdown.AddTime (sunring.addSeconds);
	}

	void onGameTimeDepleted(){
		StopGameTimers ();
		GetComponent<AudioSource>().Play();
//		Destroy (ovrPlayer.gameObject);
	}

	public void RecenterHMD(){
		
	}

	IEnumerator WaitForHSWDisplay(){
		while (OVRManager.isHSWDisplayed)
			yield return null;
		StartGameTimers ();
	}

	void StartGameTimers(){
		elapsed.ResetTimer ();
		countdown.ResetTimer ();

		elapsed.StartTimer ();
		countdown.StartTimer ();
	}

	void StopGameTimers(){
		elapsed.StopTimer ();
		countdown.StopTimer ();
	}
}
