﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameTimer : MonoBehaviour {
	
	public float Seconds;
	public Text lblTimer;
	public bool countUp = false;
	public string formatString = "00.00";
	
	public float elapsed;
	
	bool isRunning = false;

	void Start () {
		ResetTimer ();
	}
	
	void FixedUpdate () {
		if (isRunning) {
			if(countUp){
				elapsed += Time.fixedDeltaTime;
			}else{
				elapsed -= Time.fixedDeltaTime;
			}
			if(elapsed <= 0){
				elapsed = 0;
				isRunning = false;
				NotifyTimerDepleted();
			}
		}
		lblTimer.text = elapsed.ToString (formatString);
	}

	public void AddTime(float seconds){
		elapsed += seconds;
	}
	
	public void StartTimer(){
		isRunning = true;
	}
	
	public void StopTimer(){
		isRunning = false;
	}
	
	public void ResetTimer(){
		elapsed = Seconds;
	}
	
	public void NotifyTimerDepleted(){
		NotificationCenter.DefaultCenter.PostNotification (this, "onGameTimeDepleted");
	}
	
}
