﻿using UnityEngine;
using System.Collections;

public class SunringCycler : MonoBehaviour {

	SunRingCollect[] sunrings;
	SunRingCollect activeRing;
	
	void Awake () {
//		Debug.Log ("Starting sunring cycler...");
//		Debug.Log (sunrings.Length);
		sunrings = gameObject.GetComponentsInChildren<SunRingCollect> ();
		foreach (SunRingCollect col in sunrings) {
			col.gameObject.SetActive(false);
		}
		activeRing = Extensions.RandomElementFromArray<SunRingCollect> (sunrings);
		activeRing.gameObject.SetActive (true);
		NotificationCenter.DefaultCenter.AddObserver (this, "onPlayerCollectedSunring");
	}

	void onPlayerCollectedSunring () {
//		Debug.Log ("new sunring");
		GetComponent<AudioSource>().Play();
		activeRing.gameObject.SetActive(false);
		activeRing = Extensions.RandomElementFromArray(sunrings);
//		Debug.Log (activeRing);
//		Debug.Log (activeRing.gameObject.activeSelf);
//		Debug.Log (activeRing.isActiveAndEnabled);
		activeRing.gameObject.SetActive (true);
//		Debug.Log (activeRing.gameObject.activeSelf);
//		Debug.Log (activeRing.isActiveAndEnabled);
	}
}
