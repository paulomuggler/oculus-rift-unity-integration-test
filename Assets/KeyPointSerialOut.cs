﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.IO;

public class KeyPointSerialOut : MonoBehaviour {

	string serialPortName;
	int serialBaudRate;
	SerialPort stream;

	float serialGuardTimer = 0;
	public int serialGuardTime = 100;

	bool rec = false;
	string serialIn = "no data.";

	SphereCollider keyPointZone;
	Transform player;

	// Use this for initialization
	void Start () {
		serialPortName = File.ReadAllLines (Application.dataPath+"/DesafiandoAGravidade/serialPortName.txt")[0];
		serialBaudRate = int.Parse(File.ReadAllLines (Application.dataPath+"/DesafiandoAGravidade/serialBaudRate.txt")[0]);

		stream = new SerialPort(serialPortName, serialBaudRate);
		stream.Open();

		stream.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

		keyPointZone = GetComponent<SphereCollider> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		serialGuardTimer = Time.time;
	}
	
	void OnTriggerStay () {
		Vector3 playerPosition = player.position;
		Vector3 zoneCenter = keyPointZone.transform.position;

		float distMag = Vector3.Distance (playerPosition, zoneCenter);
		float zoneRadius = keyPointZone.radius;

		float insideZone = Extensions.Map (zoneRadius, 0, 0, 1, distMag);
		/*
		string[] vals = new string[3];
		vals [0] = "val0";
		vals [1] = "val1";
		vals [2] = "val2";

		stream.WriteLine (vals [0]);
		stream.WriteLine (vals [1]);
		stream.WriteLine (vals [2]);

		Debug.Log ("Serial Data Sent: "+vals.ToString());
		 */
		string payload = "testing serial connection";
		if(Time.time - serialGuardTimer >= serialGuardTime){
			serialGuardTimer = Time.time;
			stream.WriteLine (payload);
		}
		//Debug.Log ("Serial Data Sent: "+payload);

	}

	private static void DataReceivedHandler(
		object sender,
		SerialDataReceivedEventArgs e)
	{
		SerialPort sp = (SerialPort)sender;
		string indata = sp.ReadExisting();
		Debug.Log("Data Received:");
		Debug.Log(indata);
	}

	
	void Update(){
		//if (rec) {
		//serialIn = stream.ReadLine ();
		
		//Debug.Log("Serial data received: "+serialIn);
			//rec = false;
			//serialIn = "no data.";
		//}
	}
}
